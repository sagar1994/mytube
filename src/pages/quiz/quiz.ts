import { Component, ViewChild  } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database'
import { Observable } from 'rxjs-compat';
import { timeout } from 'rxjs/operators';
@Component({
  selector: 'page-quiz',
  templateUrl: 'quiz.html'
})
export class Quizpage {
  storyitems: Observable<any[]>;
  @ViewChild('slides') slides: any;

    slideOptions: any;
    flashCardFlipped: boolean = false;

    // static data
  public imageUrl = 'https://firebasestorage.googleapis.com/v0/b/lovely-sardhav.appspot.com/o/helicopter.png?alt=media&token=b81b06b1-db32-4be7-abf3-4f7531c21d76';
  public rightAnswer = 'Helicopter';
  public question = 'What is this?';
  public option1 = 'Helicoptor';
  public option2 = 'Truck';
  public option3 = 'Plane';
  public option4 = 'Bus';

  public questionsArray =  [{imageUrl: 'https://firebasestorage.googleapis.com/v0/b/lovely-sardhav.appspot.com/o/helicopter.png?alt=media&token=b81b06b1-db32-4be7-abf3-4f7531c21d76'
, question: 'what is second question?',
option1: 'ek',
option2: 'be',
option3: 'tran',
option4: 'chaar',
rightAnswer: 'chaar'},
{imageUrl: 'https://firebasestorage.googleapis.com/v0/b/lovely-sardhav.appspot.com/o/helicopter.png?alt=media&token=b81b06b1-db32-4be7-abf3-4f7531c21d76'
, question: 'what is third question?',
option1: '2ek',
option2: '2be',
option3: '2tran',
option4: '2chaar',
rightAnswer: '2chaar'},
{imageUrl: 'https://firebasestorage.googleapis.com/v0/b/lovely-sardhav.appspot.com/o/helicopter.png?alt=media&token=b81b06b1-db32-4be7-abf3-4f7531c21d76'
, question: 'what is fourth question?',
option1: '3ek',
option2: '3be',
option3: '3tran',
option4: '3chaar',
rightAnswer: '3chaar'}];
public count = 0;

  constructor(public navCtrl: NavController, public database: AngularFireDatabase) {
    this.getStoriesFromDatabase();
    console.log('Hello FlashCardComponent Component');
  }

  getStoriesFromDatabase() {
    // const myVideosRef = this.database.list('stories');
  this.storyitems=this.database.list('stories').valueChanges();
    console.log('items is ======>',this.storyitems);
  }

  ionViewDidLoad() {

}

selectAnswer(){
    this.flashCardFlipped = true;
    // this.imageUrl = this.questionsArray[this.count].imageUrl;
    // this.question = this.questionsArray[this.count].question;
    // this.option1 = this.questionsArray[this.count].option1;
    // this.option2 = this.questionsArray[this.count].option2;
    // this.option3 = this.questionsArray[this.count].option3;
    // this.option4 = this.questionsArray[this.count].option4;
    // this.rightAnswer = this.questionsArray[this.count].rightAnswer;
    // this.count = this.count + 1;
    setTimeout(()=>{
        this.imageUrl = this.questionsArray[this.count].imageUrl;
    this.question = this.questionsArray[this.count].question;
    this.option1 = this.questionsArray[this.count].option1;
    this.option2 = this.questionsArray[this.count].option2;
    this.option3 = this.questionsArray[this.count].option3;
    this.option4 = this.questionsArray[this.count].option4;
    this.rightAnswer = this.questionsArray[this.count].rightAnswer;
    this.count = this.count + 1;
    if(this.count === 3){
this.count = 0;
    }
        this.flashCardFlipped = false;
  },3000)
}
}
