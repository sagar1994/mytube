import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database'
import { Observable } from 'rxjs-compat';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  storyitems: Observable<any[]>;
  constructor(public navCtrl: NavController, public database: AngularFireDatabase) {
    this.getStoriesFromDatabase();
  }

  getStoriesFromDatabase() {
    // const myVideosRef = this.database.list('stories');

  this.storyitems=this.database.list('stories').valueChanges();

    console.log('items is ======>',this.storyitems);


  }
}
