import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AngularFireDatabase} from '@angular/fire/database';
import { AngularFireAuth } from 'angularfire2/auth';
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
error_message = '';
success_message = '';
user={
    fullname : "",
    email : "",
    comments :""
  };

  constructor(private afAuth: AngularFireAuth,public navCtrl: NavController, public database:AngularFireDatabase) {
console.log('afAuth is ========>', afAuth.auth.currentUser);
  }

  logForm(){
  		if(this.user.fullname != '' && this.user.comments != ''){
  		console.log(this.user);
      	const userRef= this.database.list('users');
      	userRef.push(this.user);
      	this.error_message = '';
      	this.user.fullname = '';
      	this.user.email = '';
      	this.user.comments = '';
      	this.success_message = 'demo text';
      	setTimeout(() => this.success_message = '', 3000);
      	this.navCtrl.parent.select(1);	
  		} else {
  		setTimeout(() => this.error_message = '', 3000);
  		this.error_message = 'Fill All The Fields Properly'
  		}
      
      
    }

}
