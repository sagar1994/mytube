import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule }    from '@angular/http';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { Quizpage } from '../pages/quiz/quiz';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {YoutubePipe} from '../pipes/youtube/youtube';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireDatabaseModule, AngularFireDatabase} from 'angularfire2/database';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import {InAppBrowser} from '@ionic-native/in-app-browser'
import { FlashCardComponent } from '../components/flash-card/flash-card';

import {GooglePlus} from '@ionic-native/google-plus';
// import { Data } from '../providers/data';


var config = {
  apiKey: "AIzaSyA3GJdP1KVJcUvZgHpgPN3ewQy8D-fa7Lw",
  authDomain: "lovely-sardhav.firebaseapp.com",
  databaseURL: "https://lovely-sardhav.firebaseio.com",
  projectId: "lovely-sardhav",
  storageBucket: "lovely-sardhav.appspot.com",
  messagingSenderId: "224539240512"
};
@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    YoutubePipe,
    FlashCardComponent,
    Quizpage,
  ],
  imports: [
    BrowserModule,
    AngularFireDatabaseModule,
    AngularFireModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    Quizpage,
  ],
  providers: [
    StatusBar,
    Device,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    InAppBrowser,
    GooglePlus
  ]
})
export class AppModule {}
