import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { Quizpage } from '../quiz/quiz';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  flag = true;
  // isUserLoggedIn=false;
  tab1Root = HomePage;
  tab2Root = ContactPage;
  tab3Root = AboutPage;
  tab4Root = Quizpage;
  
  constructor(public alertCtrl: AlertController,
    public navCtrl: NavController,
    private afAuth: AngularFireAuth) {
      if(afAuth.auth.currentUser){
        this.flag = true;
      }

  }
}
