import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AngularFireDatabase } from '@angular/fire/database'
import { Observable } from 'rxjs-compat';
import { Device } from '@ionic-native/device';
import { AlertController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { GooglePlus } from '@ionic-native/google-plus';
import { Platform } from 'ionic-angular';
// import { Data } from '../../providers/data';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  user: Observable<firebase.User>;
  @ViewChild('slides') slides: any;
  slideOptions: any;
  flashCardFlipped: boolean = false;
  items: Observable<any[]>;
  userdetailsref: any;
  userDetails: any;
  profilePic = 'https://firebasestorage.googleapis.com/v0/b/lovely-sardhav.appspot.com/o/helicopter.png?alt=media&token=b81b06b1-db32-4be7-abf3-4f7531c21d76';
  displayName = 'Guest User';

  ngOnInit(): void {

  }
  constructor(private afAuth: AngularFireAuth,
    private gplus: GooglePlus,
    private platform: Platform,
    public navCtrl: NavController, public database: AngularFireDatabase, private device: Device
    , public alertCtrl: AlertController, public inAppBrowser: InAppBrowser) {
      
      // google auth
      this.user = this.afAuth.authState;
      console.log('user auth is ======', this.user);
      //--------------------------------------------------------
      this.getVideosFromDatabase()
      const devicesRef = this.database.list('devices');
      this.userdetailsref = this.database.list('userdetails');
      if(localStorage.getItem('profilepic') && localStorage.getItem('username')){
        this.profilePic = localStorage.getItem('profilepic');
        this.displayName = localStorage.getItem('username');
      }
    console.log('Device UUID is ====>: ' + this.device.uuid);
    console.log('Device MODEL is ====>: ' + this.device.model);
    console.log('Device PLATFORM is ====>: ' + this.device.platform);
    console.log('Device VERSION is ====>: ' + this.device.version);
    console.log('Device MANUFACTURER is ====>: ' + this.device.manufacturer);
    console.log('Device SERIAL is ====>: ' + this.device.serial);
    if (this.device.uuid != null) {
      let deviceInfo = {
        "uuid": this.device.uuid,
        "model": this.device.model,
        "platform": this.device.platform,
        "version": this.device.version,
        "manufacturer": this.device.manufacturer,
        "serial": this.device.serial
      }
      devicesRef.set(deviceInfo.uuid, deviceInfo);
      // devicesRef.push(this.device);
    }


  }

  videos: any[] = [
    {
      'title': 'Local Bijli Aur Bill Ft. Shahid Kapoor | Ashish Chanchlani ',
      'url': 'https://m.youtube.com/watch?v=VD53-dKZQuA'
    },
    {
      'title': 'Amitabh Bachchan | Khudabaksh | Thugs of Hindostan ',
      'url': 'https://www.youtube.com/embed/iFr8Xkt3j08'
    }
  ];

  googleLogin() {
    console.log("google login called ========>");
    if (this.platform.is('cordova')) {
      console.log("google login called ========>1");
      this.nativeGoogleLogin();
    } else {
      console.log("google login called ========>2");
      this.webGoogleLogin ();
    }
  }

  // googleLogin(): Promise<any> {
  //   return new Promise((resolve, reject) => { 
  //       this.gplus.login({
  //         'webClientId': '5351366995-npuh9q89gaoiagoc4jssqck26gorj7hh.apps.googleusercontent.com',
  //         'offline': true
  //       }).then( res => {
  //               const googleCredential = firebase.auth.GoogleAuthProvider
  //                   .credential(res.idToken);
  
  //               firebase.auth().signInWithCredential(googleCredential)
  //             .then( response => {
  //                 console.log("Firebase success: " + JSON.stringify(response));
  //                 resolve(response)
  //             });
  //       }, err => {
  //           console.error("Error: ", err)
  //           reject(err);
  //       });
  //     });
  //     }

  async nativeGoogleLogin(): Promise<any> {
    try {
      console.log('native login');
      const gplusUser = await this.gplus.login({
        'webClientId': '224539240512-8iffsr3lt4281c2kus3uiupq6uhgbq7h.apps.googleusercontent.com',
        'offline': true,
        'scopes': 'profile email'
      })
     return await this.afAuth.auth.signInWithCredential(
        firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken)
      )
    } catch (err) {
      alert(err);
      console.log(err);
    }
  }

  async webGoogleLogin(): Promise<void> {
    try {
      console.log("google login called ========>3");
      const provider = new firebase.auth.GoogleAuthProvider();
      console.log("google login called ========>4", provider);
      const credentials = await this.afAuth.auth.signInWithPopup(provider);
      console.log("google login called ========>5", credentials);
      this.userDetails = credentials.user;
      console.log('displayName =====>', this.userDetails.displayName);
      console.log('email =====>', this.userDetails.email);
      console.log('photoURL =====>', this.userDetails.photoURL);
      console.log('uid =====>', this.userDetails.uid);
      let userInfo = {
        "fullname": this.userDetails.displayName || 'no name',
        "email": this.userDetails.email || 'no email',
        "photoURL": this.userDetails.photoURL  || 'https://firebasestorage.googleapis.com/v0/b/lovely-sardhav.appspot.com/o/helicopter.png?alt=media&token=b81b06b1-db32-4be7-abf3-4f7531c21d76',
        "uid": this.userDetails.uid || '0000'
      }
      this.profilePic = this.userDetails.photoURL;
      this.displayName = this.userDetails.displayName;
      localStorage.setItem('profilepic', this.userDetails.photoURL);
      localStorage.setItem('username', this.userDetails.displayName);
      console.log(this.profilePic);
      var myUserId = firebase.auth().currentUser.uid;
      if(this.userDetails.phoneNumber){
        userInfo['phone'] = this.userDetails.phoneNumber;
      }
      this.userdetailsref.set(userInfo.uid, userInfo);
    } catch (err) {
      console.log(err);
    }
  }

  signOut() {
    this.afAuth.auth.signOut();
      localStorage.removeItem('profilepic');
      localStorage.removeItem('username');
      this.profilePic = 'https://firebasestorage.googleapis.com/v0/b/lovely-sardhav.appspot.com/o/helicopter.png?alt=media&token=b81b06b1-db32-4be7-abf3-4f7531c21d76';
      this.displayName = 'Guest User'; 
    if (this.platform.is('cordova')) {
      this.gplus.logout();
    }
  }
  getVideosFromDatabase() {
    const myVideosRef = this.database.list('myVideos');

    this.items = this.database.list('myVideos').valueChanges();

    console.log(this.items);


  }

  openVideo(url) {
    const options: InAppBrowserOptions = {
      zoom: "no"

    }
    const browser = this.inAppBrowser.create(url, "_self", options);

  }

}
